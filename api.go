package main

type ReverseGeocodeRequest struct {
	Lat float64 `query:"lat"`
	Lon float64 `query:"lon"`
}

type ReverseGeocodeResponse struct {
	State   string `json:"state,omitempty"`
	City    string `json:"city,omitempty"`
	Country string `json:"country,omitempty"`
}
