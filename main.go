package main

import (
	"log/slog"
	"os"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/lmittmann/tint"
	"github.com/sams96/rgeo"
	"github.com/twpayne/go-geom"
)

var rg *rgeo.Rgeo

func main() {
	app := fiber.New()
	app.Use(logger.New())

	app.Get("/reverse-geocode", reverseGeocodeHandler)

	slog.SetDefault(slog.New(
		tint.NewHandler(os.Stdout, &tint.Options{
			Level:      slog.LevelDebug,
			TimeFormat: time.Kitchen,
		}),
	))

	var err error

	rg, err = rgeo.New(rgeo.Cities10, rgeo.Provinces10)
	if err != nil {
		panic(err)
	}

	slog.Info("rgeo fully initialized!")

	app.Listen(":3000")
}

func reverseGeocodeHandler(c *fiber.Ctx) error {
	var req ReverseGeocodeRequest

	if err := c.QueryParser(&req); err != nil {
		return err
	}

	loc, err := rg.ReverseGeocode(geom.Coord{
		// NOTE: longitude first
		req.Lon,
		req.Lat,
	})
	if err != nil {
		return err
	}

	return c.JSON(ReverseGeocodeResponse{
		State:   loc.Province,
		City:    loc.City,
		Country: loc.Country,
	})
}
